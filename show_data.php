<?php 
    $DB_NAME        = "kampus";
    $DB_USER        = "root";
    $DB_PASS        = "";
    $DB_SERVER_LOC  = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
        $nama = $_POST['nama'];
        $sql = " SELECT m.nim, m.nama,
                p.nama_prodi, concat('http://192.168.43.81/kampus/images/', photos) as url
                FROM mahasiswa m, prodi p
                WHERE m.id_prodi = p.id_prodi and m.nama like '%$nama%'";
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_mhs = array();
            while ($mhs = mysqli_fetch_assoc($result)) {
                array_push($data_mhs, $mhs);
            }
            echo json_encode($data_mhs);
        }
    }
?>

      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Dashboard Obat</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Obat</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>


      <div class="row">
        <div class="col-md-6">
          <a href="index.php?page=tambahobat" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Obat</a>
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Obat</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>NIM</th>
                    <th>Nama </th>
                    <th>Prodi</th>
                    <th>Photos</th>
                    <th>Tindakan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
              $query = mysqli_query($koneksi, "SELECT mahasiswa.nim, mahasiswa.nama, prodi.nama_prodi, mahasiswa.photos
              FROM mahasiswa, prodi
              WHERE mahasiswa.id_prodi = prodi.id_prodi");
              $jumlah = mysqli_num_rows($query);
              while ($data = mysqli_fetch_array($query)){
              ?>
                  <tr>
                    <td><?php echo $data['nim'] ?></td>
                    <td><?php echo $data['nama'] ?></td>
                    <td><?php echo $data['nama_prodi'] ?></td>
                    <td><?php echo $data['photos'] ?></td>
                    <td>
                      <a href="index.php?page=editobat&id_obat=<?php echo $data['id_obat']; ?>"
                          class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>
                      <a href="index.php?page=hapusobat&id_obat=<?php echo $data['id_obat']; ?>"
                       class="btn btn-danger btn-sm delete-link"><i class="fa fa-trash"></i> Hapus</a>
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
                <tfoot>
                  <tr>
                  <th>NIM</th>
                    <th>Nama </th>
                    <th>Prodi</th>
                    <th>Photos</th>
                    <th>Tindakan</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>