<?php 
include ("config.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>LIST MAHASISWA</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<ul>
  <li><a class="active" >DAFTAR MAHASISWA</a></li>
</ul>
	<center>
	<br>
		<table class="table1">
			<thead>
				<tr>
					<th> NIM </th>
					<th> Nama </th>
					<th> Prodi </th>
					<th> Foto </th>
					<th> Tindakan </th>
				</tr>
			</thead>
			<tbody>
				<?php
					$sql = "SELECT mahasiswa.nim, mahasiswa.nama, prodi.nama_prodi, mahasiswa.photos
					FROM mahasiswa, prodi
					WHERE mahasiswa.id_prodi = prodi.id_prodi";
					$query = mysqli_query($db, $sql);
					while($siswa = mysqli_fetch_array($query)){
						?>
						<tr>
								<td><?php echo $siswa['nim']; ?></td>
								<td><?php echo $siswa['nama']; ?></td>
								<td><?php echo $siswa['nama_prodi']; ?></td>
								<td>
								<img src="images/<?php echo $siswa['photos']?>" width='100'>
								</td>
								<td>
										<a class="buttondelete buttonr buttons" href="hapus.php?nim=<?php echo $siswa['nim']; ?>">Hapus</a>

							</td>
						</tr>
					<?php } ?>
			</tbody>
		</table>
</center>
	</body>
</html>
